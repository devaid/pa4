#ifndef LIST_H
#define LIST_H
typedef struct Node* Node;
struct Node {
    void* value;
    Node next;
    Node prev;
};

typedef struct List* List;
struct List {
    int cursor;
    int length;
    Node front;
    Node back;
    Node current;
};

// Constructors-Destructors ---------------------------------------------------
List newList(void); // Creates and returns a new empty List.
void freeList(List* l); // Frees all heap memory associated with *pL, and sets
 // *pL to NULL.
// Access functions -----------------------------------------------------------
int length(List l); // Returns the number of elements in L.
int index(List l); // Returns index of cursor element if defined, -1 otherwise.
Node front(List l); // Returns front element of L. Pre: length()>0
Node back(List l); // Returns back element of L. Pre: length()>0
Node get(List l); // Returns cursor element of L. Pre: length()>0, index()>=0

// Manipulation procedures ----------------------------------------------------
void clearList(List l); // Resets L to its original empty state.
void set(List l, void* x); // Overwrites the cursor element’s data with x.
 // Pre: length()>0, index()>=0
void moveFront(List l); // If L is non-empty, sets cursor under the front element,
 // otherwise does nothing.
void moveBack(List l); // If L is non-empty, sets cursor under the back element,
 // otherwise does nothing.
void movePrev(List l); // If cursor is defined and not at front, move cursor one
 // step toward the front of L; if cursor is defined and at
 // front, cursor becomes undefined; if cursor is undefined
 // do nothing
void moveNext(List l); // If cursor is defined and not at back, move cursor one
 // step toward the back of L; if cursor is defined and at
 // back, cursor becomes undefined; if cursor is undefined
 // do nothing

void prepend(List l, void* x); // Insert new element into L. If L is non-empty,
 // insertion takes place before front element.
void append(List l, void* x); // Insert new element into L. If L is non-empty,
 // insertion takes place after back element.
void insertBefore(List l, void* x); // Insert new element before cursor.
 // Pre: length()>0, index()>=0
void insertAfter(List l, void* x); // Insert new element after cursor.
 // Pre: length()>0, index()>=0
void deleteFront(List l); // Delete the front element. Pre: length()>0
void deleteBack(List l); // Delete the back element. Pre: length()>0
void delete(List l); // Delete cursor element, making cursor undefined.
 // Pre: length()>0, index()>=0
// Other operations -----------------------------------------------------------
#endif