#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>
#include <stddef.h>
#include "List.h"
#include "Matrix.h"

struct Entry* newEntry(int i, double v) {
    Entry e = malloc(sizeof(int) + sizeof(double));
    e->index = i;
    e->value = v;
    return e;
};

struct Matrix* newMatrix(int n) {
    Matrix M = malloc(n * sizeof(List) + 3 * sizeof(int));
    M->numRows = n;
    M->nnz = 0;
    for (int i = 0; i < n; i++) {
        M->rows[i] = newList();
    }
    return M;
};

void freeMatrix(Matrix *pM) {
    free(*pM);
    pM = NULL;
};

int size(Matrix M) {
    return M->numRows;
};

int NNZ(Matrix M) {
    return M->nnz;
}

void makeZero(Matrix M) {
    for(int i = 0; i < M->numRows; i++) {
        List l = M->rows[i];
        M->rows[i] = newList();
        clearList(l);
    }
    M->nnz = 0;
};

void changeEntry(Matrix M, int i, int j, double x) {
    List l = M->rows[i];
    moveFront(l);
    int changed = 0;
    while(get(l) != NULL) {
        Entry e = get(l)->value;
        if (e->index == j) {
            set(l, newEntry(j, x));
            changed = 1;
            if (x == 0) {
                delete(l);
                M->nnz--;
            }
            break;
        }
        moveNext(l);
    }
    if (changed == 0 && x != 0) {
        Entry y = newEntry(j, x);
        append(l, y);
        M->nnz++;
    }
};

void addEntry(Matrix M, int i, int j, double x) {
    List l = M->rows[i];
    moveFront(l);
    int changed = 0;
    while(get(l) != NULL) {
        Entry e = get(l)->value;
        if (e->index == j) {
            set(l, newEntry(j,  e->value + x));
            changed = 1;
            if (e->value + x == 0) {
                delete(l);
                M->nnz--;
            }
            break;
        }
        moveNext(l);
    }
    if (changed == 0) {
        Entry y = newEntry(j, x);
        append(l, y);
        M->nnz++;
    }
};

Matrix copy(Matrix A) {
    Matrix M = newMatrix(A->numRows);
    for (int i = 0; i < A->numRows; i++) {
        moveFront(A->rows[i]);
        while (get(A->rows[i]) != NULL) {
            Entry n = get(A->rows[i])->value;
            Entry e = newEntry(n->index, n->value);
            append(M->rows[i], e);
            moveNext(A->rows[i]);
        }
    }
    M->nnz = A->nnz;
    return M;
};

Matrix transpose(Matrix A) {
    Matrix M = newMatrix(A->numRows);
    for (int i = 0; i < A->numRows; i++) {
        moveFront(A->rows[i]);
        while (get(A->rows[i]) != NULL) {
            Entry n = get(A->rows[i])->value;
            Entry e = newEntry(i, n->value);
            append(M->rows[n->index], e);
            moveNext(A->rows[i]);
        }
    }
    M->nnz = A->nnz;
    return M;
};

Matrix scalarMult(double x, Matrix A) {
    Matrix M = newMatrix(A->numRows);
    for (int i = 0; i < A->numRows;i++) {
        moveFront(A->rows[i]);
        while (get(A->rows[i]) != NULL) {
            Entry e = get(A->rows[i])->value;
            Entry e2 = newEntry(e->index, e->value * x);
            append(M->rows[i], e2);
            moveNext(A->rows[i]);
        }
    }
    M->nnz = A->nnz;
    return M;
};

Matrix sum(Matrix A, Matrix B) {
    Matrix M = copy(A);
    for (int i = 0; i < B->numRows; i++) {
       moveFront(B->rows[i]);
        while (get(B->rows[i]) != NULL) {
            Entry e = get(B->rows[i])->value;
            addEntry(M, i, e->index, e->value);
            moveNext(B->rows[i]);
        }
    }
    return M;
};

Matrix diff(Matrix A, Matrix B) {
    return sum(A, scalarMult(-1.0, B));
};

int equals(Matrix A, Matrix B) {
    return NNZ(diff(A, B)) == 0;
}

double vectorDot(List A, List B) {
    moveFront(A);
    double prod = 0;
    while (get(A) != NULL) {
        moveFront(B);
        while (get(B) != NULL) {
            Entry ea = get(A)->value;
            Entry eb = get(B)->value;
            if (eb->index == ea->index) {
                prod = prod + ea->value * eb->value;
                break;
            }
            moveNext(B);
        }
        moveNext(A);
    }
    return prod;
};

Matrix product(Matrix A, Matrix B) {
    Matrix M = newMatrix(A->numRows);
    Matrix c = transpose(B);
    for (int i = 0; i < A->numRows; i++) {
        for (int j = 0; j < B->numRows; j++) {
            if (length(A->rows[i]) > 0 && length(c->rows[i]) > 0) {
                changeEntry(M, i, j, vectorDot(A->rows[i], c->rows[j]));
            }
        }
    }
    return M;
};

void printList(List l) {
    Node x = front(l);
    while (x != NULL) {
        Entry e = x->value;
        printf(" (%d,%.1f) ", e->index, e->value);
        x = x->next;
    }
};

void printMatrix(FILE* out, Matrix M) {
    for (int i = 0; i < M->numRows; i++) {
        if (length(M->rows[i]) > 0) {
            printf("%d:", i);
            printList(M->rows[i]);
            printf("\n");
        }
    }
    printf("\n");
};


