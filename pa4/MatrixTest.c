#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>
#include<stddef.h>
#include"List.h"
#include"Matrix.h"
     
int main(int argc, char **argv){
   int n=100000;
   Matrix A = newMatrix(n);
   Matrix B = newMatrix(n);
   Matrix C, C2, C3, D, D2, E, E2, E3, F, F2, G, G2, H, H2;

   changeEntry(A, 1,1,1); changeEntry(B, 1,1,1);
   changeEntry(A, 1,2,2); changeEntry(B, 1,2,0);
   changeEntry(A, 1,3,3); changeEntry(B, 1,3,1);
   changeEntry(A, 2,1,4); changeEntry(B, 2,1,0);
   changeEntry(A, 2,2,5); changeEntry(B, 2,2,1);
   changeEntry(A, 2,3,6); changeEntry(B, 2,3,0);
   changeEntry(A, 3,1,7); changeEntry(B, 3,1,1);
   changeEntry(A, 3,2,8); changeEntry(B, 3,2,1);
   changeEntry(A, 3,3,9); changeEntry(B, 3,3,1);

   printf("%d\n", NNZ(A));
   printMatrix(stdout, A);
   printf("\n");

   printf("%d\n", NNZ(B));
   printMatrix(stdout, B);
   printf("\n");

   C = scalarMult(1.5, A);
   printf("%d\n", NNZ(C));
   printMatrix(stdout, C);
   printf("\n");

   C2 = scalarMult(-1.5, A);
   printf("%d\n", NNZ(C2));
   printMatrix(stdout, C2);
   printf("\n");

   C3 = scalarMult(0, A);
   printf("%d\n", NNZ(C3));
   printMatrix(stdout, C3);
   printf("\n");

   D = sum(A, B);
   printf("%d\n", NNZ(D));
   printMatrix(stdout, D);
   printf("\n");

   D2 = sum(A, newMatrix(n));
   printf("%d\n", NNZ(D2));
   printMatrix(stdout, D2);
   printf("\n");


   E = diff(A, A);
   printf("%d\n", NNZ(E));
   printMatrix(stdout, E);
   printf("\n");
   printf("%s\n", equals(E, newMatrix(n))?"true":"false" );

   E2 = diff(A, B);
   printf("%d\n", NNZ(E2));
   printMatrix(stdout, E2);
   printf("\n");

   E3 = diff(B, A);
   printf("%d\n", NNZ(E3));
   printMatrix(stdout, E3);
   printf("\n");
   printf("%s\n", equals(E2, scalarMult(-1.0, E3))?"true":"false" );

   F = transpose(B);
   printf("%d\n", NNZ(F));
   printMatrix(stdout, F);
   printf("\n");

   F2 = transpose(transpose(A));
   printf("%d\n", NNZ(F2));
   printMatrix(stdout, F2);
   printf("\n");

   printf("%s\n", equals(A, F2)?"true":"false" );

   G = product(B, B);
   printf("%d\n", NNZ(G));
   printMatrix(stdout, G);
   printf("\n");

   G2 = product(A, B);
   printf("%d\n", NNZ(G2));
   printMatrix(stdout, G2);
   printf("\n");

   H = copy(A);
   printf("%d\n", NNZ(H));
   printMatrix(stdout, H);
   printf("\n");

   H2 = copy(newMatrix(n));
   printf("%d\n", NNZ(H));
   printMatrix(stdout, H);
   printf("\n");

   printf("%s\n", equals(A, H)?"true":"false" );
   printf("%s\n", equals(A, B)?"true":"false" );
   printf("%s\n", equals(A, A)?"true":"false" );

   makeZero(A);
   printf("%d\n", NNZ(A));
   printMatrix(stdout, A);

   printf("DONE!");

   freeMatrix(&A);
   freeMatrix(&B);
   freeMatrix(&C);
   freeMatrix(&D);
   freeMatrix(&E);
   freeMatrix(&F);
   freeMatrix(&G);
   freeMatrix(&H);

   return EXIT_SUCCESS;
}