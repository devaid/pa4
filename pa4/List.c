#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>
#include <stddef.h>
#include "List.h"

struct Node* newNode(void* val) {
    Node n = malloc(sizeof(val) + 2 * (sizeof(Node)));
    n->value = val;
    n->next = NULL;
    n->prev = NULL;
    return n;
}

void clearNode(Node n) {
    free(n);
    n = NULL;
}

struct List* newList(void) {
    List s = malloc(2 * sizeof(int) + 3 * sizeof(Node));
    s->cursor = -1;
    s->length = 0;
    s->front = NULL;
    s->back = NULL;
    s->current = NULL;
    return s;
}

int length(List l) {
    return l->length;
}

int index(List l) {
    return l->cursor;
}

Node front(List l) {
    if (length(l) > 0) {
        return l->front;
    }
    return NULL;
}

Node back(List l) {
    if (length(l) > 0) {
        return l->back;
    }
    return NULL;
}

Node get(List l) {
    if (length(l) > 0 && index(l) != -1) {
        return l->current;
    }
    return NULL;
}

void set(List l, void* x) {
    if (index(l) != -1) {
        l->current->value = x;
    }
}

void moveFront(List l) {
    l->cursor = 0;
    l->current = front(l);
}

void moveBack(List l) {
    l->cursor = length(l) - 1;
    l->current = back(l);
}

void movePrev(List l) {
    if (l->cursor <= 0) {
        return;
    }
    l->cursor--;
    l->current = l->current->prev;
}

void moveNext(List l) {
    if (l->cursor == -1 || l->cursor >= length(l)) {
        return;
    }
    l->cursor++;
    l->current = l->current->next;
}

void clearList(List l) {
    Node x = l->front;
    while (x != NULL) {
        Node y = x->next;
        clearNode(x);
        x = y;
    }
    free(l);
    l = NULL;
}

void append(List l, void* value) {
    if (value == 0) {
        if (l->back != NULL) {
            delete(l);
        }
        return;
    }
    Node n = newNode(value);
    if(l->back == NULL) {
        l->front = n;
        l->back = n;
        l->length = 0;
        l->cursor = 0;
        l->current = l->front;
    } else {
        Node temp = l->back;
        l->back = n;
        n->prev = temp;
        temp->next = n;
    }
    l->length++;
}

void prepend(List l, void* value) {
    if(length(l) == 0) {
        append(l, value);
    } else {
        Node n = newNode(value);
        n->next = l->front;
        l->front->prev = n;
        l->length++;
    }
}

void insertBefore(List l, void* value) {
    if(length(l) == 0) {
        append(l, value);
    } else {
        Node n = l->current;
        Node old = n->prev;
        Node n2 = newNode(value);
        old->next = n2;
        n2->prev = old;
        n2->next = n;
        n->prev = n2;
        l->length++;
    }
}

void insertAfter(List l, void* value) {
    if(length(l) == 0) {
        append(l,value);
    } else {
        Node n = l->current;
        Node old = n->next;
        Node n2 = newNode(value);
        old->prev = n2;
        n2->next = old;
        n2->prev = n;
        n->next = n2;
        l->length++;
    }
}

void deleteFront(List l) {
    Node n = l->front;
    l->front = l->front->next;
    l->length--;
    if(l->cursor > 0) {
        l->cursor--;
    }
    clearNode(n);
}

void deleteBack(List l) {
    Node n = l->back;
    l->back = l->back->prev;
    if (l->cursor == l->length - 1) {
        l->cursor--;
    }
    l->length--;
    clearNode(n);
}

void delete(List l) {
    if (l->cursor == -1) {
        return;
    } else {
        Node n = l->current;
        Node old = n->prev;
        Node n2 = n->next;
        if (old == NULL && n2 == NULL) {
            l->cursor = -1;
            l->current = NULL;
            l->front = NULL;
            l->back = NULL;
        } else if (old == NULL) {
            n2->prev = NULL;
            l->front = n2;
        } else if (n2 == NULL) {
            old->next = NULL;
            l->back = old;
        } else {
            old->next = n2;
            n2->prev = old;
        }
        l->length--;
        clearNode(n);
    }
}


